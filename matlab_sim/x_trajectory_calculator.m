%% RoboTrim2018
% Title: Simulation of the movement along the x-axis for a single spot
% Author: Thomas Saraby Vatle
% Dato opprettet: 12.04.2018
% Last revision: 20.05.2018

clc
clear all
close all

%% Universal constants
vf = 1; % [m/s] Velocity of the fish along the conveyor belt, positive direction is the direction of movement
tskj = 0.1; % [s] Assumed cutting time
aks = 18; % [m/s^2] Acceleration of the knife along the x-axis

%% Constants declared by user input
prompt = 'What is the starting position of the knife (in meters)? (0 < x < 1)';
x0 = input(prompt);
prompt = 'What is the starting position of the first melanin spot (in meters)? (0 < x < 1)';
xf0 = input(prompt);
prompt = 'What is the starting velocity of the knife (in meters/second)?';
vk0 = input(prompt);
prompt = 'What is the wanted cutting length (in centimeters)?';
Lk = input(prompt);

%%  calculating cutting velocity for the knife to achieve wanted cutting length
vs = 1 - (Lk/(100*tskj));

%%  Deciding the direction of acceleration
if (x0 > xf0)
    a = -aks; % [m/s^2] Acceleration of the knife (positive in the same direction the fish is moving)
    r = aks; % [m/s^2] Deceleration of the knife
elseif (xf0 > x0)
    a = aks;
    r = -aks;
end

%% Variables and equations
syms x x1 x2 xf v1 t t1 t2 % Variables in the set of equations
Eqns = [x1 == x0 + vk0*t1 + 0.5*(a)*t1^2; % Knife position after the acceleration phase
        v1 == vk0 + a*t1; % Velocity after the acceleration phase
        vs == v1 + r*t2; % Velocity when the knife and the melanin spot meet
        x2 == x1 + v1 * t2 + 0.5*r*t2^2; % Deceleration phase
        x2 == xf0 + vf*t; % Position of the melanin spot when the spot and the knife meet
        t == t1 + t2; % Time when the knife and the melanin spot meet
        x == x2 + vs*tskj % Knife position after cutting
        xf == xf0 + vf*(t+tskj)]; % Position of the melanin spot after it has been cut
    
% Solving the set of equations
[xs,x1s,x2s,xfs,v1s,ts,t1s,t2s] = vpasolve(Eqns, [x,x1,x2,xf,v1,t,t1,t2]);

% There are two solutions, but only one is valid (negative time is impossible)
% The invalid solution is removed below:
if ((ts(1) < 0) && (ts(2) < 0)) || ((t1s(1) < 0) && (t1s(2) < 0)) || ((t2s(1) < 0) && (t2s(2) < 0))
    disp('Umulig situasjon')
    T = false;
elseif (ts(1) < 0) || (t1s(1) < 0) || (t2s(1) < 0) 
    T = true;
    % Deleting impossible solution if applicable, and updating variables:
    xs = xs(2);
    x1s = x1s(2);
    x2s = x2s(2);
    xfs = xfs(2);
    v1s = v1s(2);
    ts = ts(2);
    t1s = t1s(2);
    t2s = t2s(2);
elseif (ts(2) < 0) || (t1s(2) < 0) || (t2s(2) < 0)
    T = true;
    % Deleting impossible solution if applicable, and updating variables:
    xs = xs(1);
    x1s = x1s(1);
    x2s = x2s(1);
    xfs = xfs(1);
    v1s = v1s(1);
    ts = ts(1);
    t1s = t1s(1);
    t2s = t2s(1);
end

%% Printing values of the variables to the user:
if (T == true)
    fprintf('Knivposisjon etter skj�ring:                                              %.3f m\n', xs);
    fprintf('Posisjon n�r kniv og flekk m�tes ved �nsket hastighet:                    %.3f m\n', x2s);
    fprintf('Posisjon n�r akselerasjonen snur:                                         %.3f m\n', x1s);
    fprintf('Tenkt flekkposisjon etter skj�ring:                                       %.3f m\n', xfs);
    fprintf('Knivfart n�r akselerasjonen snur:                                         %.3f m/s\n', v1s);
    fprintf('Tispunkt n�r kniv og flekk m�tes ved �nsket hastighet:                    %.3f s\n', ts);
    fprintf('Tidspunkt n�r akselerasjonen snur:                                        %.3f s\n', t1s);
    fprintf('Tid fra akslerasjonen snur, til kniv og flekk m�tes med �nsket hastighet: %.3f s\n', t2s);
else
    disp('Pr�v et annet tilfelle.')
end

%% Stopping position after braking:
    if (vs > 0)
        stopping_position = (0 - vs^2)/(2*(-aks)) + xs; 
    elseif (vs < 0)
        stopping_position = (0 - vs^2)/(2*(aks)) + xs; 
    elseif (vs == 0)
        stopping_position = xs;
    end
stopping_position