%% RoboTrim2018
% Title: Visual simulation with the given case of melanin spots
% Author: Thomas Saraby Vatle
% Date started: 02.05.2018
% Last revision: 20.05.2018

clc
clear all;
close

% Width of the melanin spots:
b = 0.05;
b_hyp = sqrt(2*(b^2)); % Smallest distance where spots don't overlap

% Number of fish
fisk = 2;

check_y = false; % Logic variable for checking if the Y-distance between spots is sufficient
check_x = false; % Logic variable for checking if the X-distance between spots is sufficient

ymin=0; % Minimum value of Y for the spots
ymax=0.25; % Largest value of Y for the spots
xmin=-0.95; % Minimum value of X for the spots
xmax=-0.35; % Largest value of X for the spots
n=3*fisk; % Number of generated spots

%% Creating an outline of the workspace
x_omr = [0, 1, 1, 0];
y_omr = [0.3, 0.3, 0, 0];
omraade = hgtransform;
patch('XData',x_omr,'YData',y_omr,'FaceColor','white','Parent',omraade)

%% Creating an outline of the fish
x_fisk = [-1, -0.3, -0.3, -1];
y_fisk = [0.3, 0.3, 0, 0];

for i = 1:fisk
    x_fisk = [-1-i, -0.3-i, -0.3-i, -1-i];
    y_fisk = [0.3 0.3 0 0];
    
    omriss(i) = hgtransform;
    patch('XData',x_fisk,'YData',y_fisk,'FaceColor',[255/255 128/255 102/255],'Parent',omriss(i))
end

%% Creating spots
% Spot 1
x_1 = [-2, -1.95, -1.95, -2];
y_1 = [0.3, 0.3, 0.25, 0.25];

g_1 = hgtransform;
patch('XData',x_1,'YData',y_1,'FaceColor',[178/255 34/255 34/255],'Parent',g_1)

% Spot 2
x_2 = [-2, -1.95, -1.95, -2];
y_2 = [0.05, 0.05, 0, 0];

g_2 = hgtransform;
patch('XData',x_2,'YData',y_2,'FaceColor',[178/255 34/255 34/255],'Parent',g_2)

% Spot 3
x_3 = [-2, -1.95, -1.95, -2];
y_3 = [0.125, 0.125, 0.175, 0.175];

g_3 = hgtransform;
patch('XData',x_3,'YData',y_3,'FaceColor',[178/255 34/255 34/255],'Parent',g_3)

% Spot 4
x_4 = [-2.35, -2.3, -2.3, -2.35];
y_4 = [0.3, 0.3, 0.25, 0.25];

g_4 = hgtransform;
patch('XData',x_4,'YData',y_4,'FaceColor',[178/255 34/255 34/255],'Parent',g_4)

% Spot 5
x_5 = [-2.675, -2.625, -2.625, -2.675];
y_5 = [0.05, 0.05, 0, 0];

g_5 = hgtransform;
patch('XData',x_5,'YData',y_5,'FaceColor',[178/255 34/255 34/255],'Parent',g_5)

% Spot 6
x_6 = [-3, -2.95, -2.95, -3];
y_6 = [0.3, 0.3, 0.25, 0.25];

g_6 = hgtransform;
patch('XData',x_6,'YData',y_6,'FaceColor',[178/255 34/255 34/255],'Parent',g_6)

%% Simulating the movement
axis equal
grid on
xlim([-1 1.5])
ylim([-0.1 0.4])

% Movement of the spots
f_dist = 1; % Distance between fish
vf1 = fisk + 3; % % Deciding the length of the simulation

pt1 = [0 0 0]; % Starting point of the first fish

vf = [vf1 0 0]; % Velocity of the fish

for t = linspace(0,1,100*vf1)
    % Movement of the fish:
    for i = 1:fisk
        omriss(i).Matrix = makehgtform('translate',pt1 + t*vf);
    end
    % Movement of the spots:
    for i = 1:3:n-2
        g_1.Matrix = makehgtform('translate',pt1 + t*vf);
        g_2.Matrix = makehgtform('translate',pt1 + t*vf);
        g_3.Matrix = makehgtform('translate',pt1 + t*vf);
        
        g_4.Matrix = makehgtform('translate',pt1 + t*vf);
        g_5.Matrix = makehgtform('translate',pt1 + t*vf);
        g_6.Matrix = makehgtform('translate',pt1 + t*vf);
        
    end
    drawnow
end