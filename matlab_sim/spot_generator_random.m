%% RoboTrim2018
% Title: Visual simulation with randomly generated melanin spots
% Author: Thomas Saraby Vatle
% Date started: 12.04.2018
% Last revision: 20.05.2018

clc
clear all;
close

% Width of the melanin spots:
b = 0.05;
b_hyp = sqrt(2*(b^2)); % Smallest distance where spots don't overlap

% Number of fish
fisk = 5;

check_y = false; % Logic variable for checking if the Y-distance between spots is sufficient
check_x = false; % Logic variable for checking if the X-distance between spots is sufficient

ymin=0; % Minimum value of Y for the spots
ymax=0.25; % Largest value of Y for the spots
xmin=-0.95; % Minimum value of X for the spots
xmax=-0.35; % Largest value of X for the spots
n=3*fisk; % Number of generated spots

%% Generating random y-coordinates for the spots
while check_y == false % Checking if the spots are too close together (overlapping spots is unwanted in this simulation)
    y_rnd=ymin+rand(1,n)*(ymax-ymin);
    for i = 1:n-2
        if (abs(y_rnd(i) - y_rnd(i+1)) < b_hyp) || (abs(y_rnd(i+1) - y_rnd(i+2)) < b_hyp) || (abs(y_rnd(i) - y_rnd(i+2)) < b_hyp)
            y_rnd=ymin+rand(1,n)*(ymax-ymin);
        else
            check_y = true;
        end
    end
end

%% Generating random x-coordinates for the spots
while check_x == false % Checking if the spots are too close togehter (overlapping spots is unwanted in this simulation)
    x_rnd=xmin+rand(1,n)*(xmax-xmin);  
    for i = 1:3:n-2
        if (abs(x_rnd(i) - x_rnd(i+1)) < b_hyp) || (abs(x_rnd(i+1) - x_rnd(i+2)) < b_hyp) || (abs(x_rnd(i) - x_rnd(i+2)) < b_hyp)
            x_rnd=xmin+rand(1,n)*(xmax-xmin);
        else
            check_x = true;
        end
    end
end

%% Creating an outline for the workspace
x_omr = [0, 1, 1, 0];
y_omr = [0.3, 0.3, 0, 0];
omraade = hgtransform;
patch('XData',x_omr,'YData',y_omr,'FaceColor','white','Parent',omraade)

%% Creating an outline of the fish
x_fisk = [-1, -0.3, -0.3, -1];
y_fisk = [0.3, 0.3, 0, 0];

for i = 1:fisk
    x_fisk = [-1-i, -0.3-i, -0.3-i, -1-i];
    y_fisk = [0.3 0.3 0 0];
    
    omriss(i) = hgtransform;
    patch('XData',x_fisk,'YData',y_fisk,'FaceColor',[255/255 128/255 102/255],'Parent',omriss(i))
end

%% Creating spots graphically
for i = 1:n
    x = [x_rnd(i), x_rnd(i) + b, x_rnd(i) + b, x_rnd(i)];
    y = [y_rnd(i) + b, y_rnd(i) + b, y_rnd(i), y_rnd(i)];

    g(i) = hgtransform;
    patch('XData',x,'YData',y,'FaceColor',[178/255 34/255 34/255],'Parent',g(i))
end

%% Simulating the movement
axis equal
grid on
xlim([-1 1.5])
ylim([-0.1 0.4])

% Movement for the spots
f_dist = 1; % Distance between fish
vf1 = fisk + 2.25; % Deciding the length of the simulation based on the number of fish

pt1 = [0 0 0]; % Starting point of the first fish

vf = [vf1 0 0]; %Velocity of the fish


for t = linspace(0,1,100*vf1)
    % Movement of the fish:
    for i = 1:fisk
        omriss(i).Matrix = makehgtform('translate',pt1 + t*vf);
    end
    % Movement of the spots:
    for i = 1:3:n-2
        g(i).Matrix = makehgtform('translate',pt1 + [-(2+i)/3 0 0] + t*vf);
        g(i+1).Matrix = makehgtform('translate',pt1 + [-(2+i)/3 0 0] + t*vf);
        g(i+2).Matrix = makehgtform('translate',pt1 + [-(2+i)/3 0 0] + t*vf);
    end
    drawnow
end