%% RoboTrim2018
% Title: Simulation of the movement along the x-axis for calculating the required acceleration of a given case
% Author: Thomas Saraby Vatle
% Date started: 16.04.2018
% Last revision: 20.05.2018

clc
clear all
close all

%% Universal constants
vf = 1; % [m/s] Velocity of the fish along the conveyor belt, positive direction is the direction of movement
tskj = 0.1; % [s] Assumed cutting time
aks = 1; % [m/s^2] Acceleration of the knife (starts at 1 and iterates until a sufficiently high acceleration is found)
spot_offset = 0; % Preliminary setting the offset distance between spots to 0
x_lim = [0 1]; % Limits of the workspace along the x-axis
legal_pos = false; % Initial value of legal position, used to check when acceleration becomes possible
T = true;

%% Constants declared by user input
prompt = 'How many melanin spots are to be removed?';
n = input(prompt);
prompt = 'What is the starting position of the knife (in meters)? (0 < x < 1)';
xk = input(prompt);
xk0 = xk;
prompt = 'What is the starting position of the first melanin spot (in meters)? (0 < x < 1)';
xf0 = input(prompt);
prompt = 'What is the starting velocity of the knife (in meters/second)?';
vk0 = input(prompt);
prompt = 'What is the wanted cutting length (in centimeters)?';
Lk = input(prompt);
fprintf('-------------------------------------------------------------------------------------------\n');
%% Constructing given case of spot offsets
offset_matrix = zeros(1, n-1);
for j = 1:(length(offset_matrix))
    prompt = 'How many centimeters behind this spot is the next one?';
    spot_offset_cm = input(prompt); % Allows the user to input offset in centimeters
    spot_offset_m = spot_offset_cm / 100; % Calculating offset in meters based on centimeter input
    fprintf('-------------------------------------------------------------------------------------------\n');
    offset_matrix(j) = spot_offset_m;
end
offset_matrix
%% Iterative calculation of required acceleration
while (legal_pos == false) || (xk > xk0) || (stopping_position > xk0)
    [aks, legal_pos, xk, stopping_position] = calculate_acceleration(n, xk, xk0, xf0, vk0, vf, tskj, aks, x_lim, Lk, offset_matrix, legal_pos, T);
    
    if (legal_pos == false) || (xk > xk0) || (stopping_position > xk0)
    aks = aks + 1;
    end % end if
end
fprintf('Required acceleration: %i m/s^2\n', aks);
%% Calculating current case
function [aks, legal_pos, xk, stopping_position] = calculate_acceleration(n, xk, xk0, xf0, vk0, vf, tskj, aks, x_lim, Lk, offset_matrix, legal_pos, T)
    for i = 0:n-1
       % Takes into account that the spots are not parallell
       if ((i > 0) && (i < n))
           xf0 = xfs - offset_matrix(i); % Starting position of the melanin spot, based on offset in the x-direction between the current and previous spot
       end % end if

       % Calling function that decides knife velocity based on wanted cutting length
       vs = cutting_speed(Lk, tskj);

       % Calling function that decides the direction of acceleration
       a = acceleration(aks, xk, xf0, vk0, vf);
       r = -a; % Deceleration

       % Calling function that calculates case 'i'
       [xs, x1s, x2s, xfs, v1s, ts, t1s, t2s] = calculations(xk, vk0, a, r, vs, xf0, vf, tskj);
       % Calling function that deletes impossible solution to the set of equations
       [xs, x1s, x2s, xfs, v1s, ts, t1s, t2s, T] = solution_picker(xs, x1s, x2s, xfs, v1s, ts, t1s, t2s, aks);
       
       if i == 0
           if vk0 > 0 
                x_turning = -((v1s)^2)/(2*(-aks)) + x1s; % Where the speed of the knife is or would be zero
           elseif vk0 < 0
                x_turning = -((v1s)^2)/(2*aks) + x1s; % Where the speed of the knife is or would be zero
           elseif vk0 == 0 && (xk <= xf0)
                x_turning = -((v1s)^2)/(2*(-aks)) + x1s;
           elseif vk0 == 0 && (xk > xf0)
                x_turning = -((v1s)^2)/(2*(aks)) + x1s; 
           end %end if
       else
           if v1s > 0 
           x_turning = -((v1s)^2)/(2*r) + x1s; % Where the speed of the knife is or would be zero
           elseif v1s < 0
           x_turning = -((v1s)^2)/(2*a) + x1s; % Where the speed of the knife is or would be zero   
           end % end if
       end % end if
        
        if x_turning < x_lim(1) && x_turning > x_lim(2)
            T = false
        end % end if        

       if (T == true)
           %Updating in such a way that the output becomes the input of the next iteration:
           xk = xs; % Ending position of the knife becomes the starting position of the next iteration
           vk0 = vs; % The ending velocity of the knife becomes the starting velocity of the next iteration
           xf0 = xfs; % Ending position of the melanin spot becomes the starting position of the next
       end % end if
       
        if (T == true)
            [legal_pos, stopping_position] = legal_position(aks, xs, x_lim, vs, x_turning);
        else
            legal_pos = false;
        end % end if
    end % end for
    
    if (legal_pos == true && T == true)
        fprintf('-------------------------------------------------------------------------------------------\n');
        fprintf('Stopping position of the knife with maximum braking after operation: %.3f m\n', stopping_position);
        fprintf('This is inside the workspace, the case is possible with the following acceleration:\n');
    elseif (legal_pos == false && T == true)
        fprintf('Stopping position of the knife with maximum braking: %.3f m\n', stopping_position);
        fprintf('This is outside of the workspace, the case is not possible with an acceleration of %i m/s^2\n', aks);
    end % end if
    
end % end function

%% Function: calculating cutting velocity for the knife to achieve wanted cutting length
function vs = cutting_speed(Lk,tskj)
vs = 1 - (Lk/(100*tskj));
end

%% Function: deciding the direction of acceleration
function a = acceleration(aks, xk, xf0, vk0, vf)
    if (xk > xf0)
        a = -aks; % [m/s^2] Acceleration of the knife (positive in the same direction the fish is moving)
    elseif (xf0 > xk) 
        a = aks;
    elseif (xf0 == xk) % If knife and spot start at the same position
        if (vk0 > vf) % Brake then accelerate
            a = -aks;
        elseif (vk0 <= vf) % Accelerate then brake
            a = aks;
        end
    end
end

%% Function: solving the set of equations describing the motion
function [xs, x1s, x2s, xfs, v1s, ts, t1s, t2s] = calculations(xk, vk0, a, r, vs, xf0, vf, tskj)
syms x x1 x2 xf v1 t t1 t2 % Variables in the set of equations
Eqns = [x1 == xk + vk0*t1 + 0.5*(a)*t1^2; % Knife position after the acceleration phase
        v1 == vk0 + a*t1; % Velocity after the acceleration phase
        vs == v1 + r*t2; % Velocity when the knife and the melanin spot meet
        x2 == x1 + v1 * t2 + 0.5*r*t2^2; % Deceleration phase
        x2 == xf0 + vf*t; % Position of the melanin spot when the spot and the knife meet
        t == t1 + t2; % Time when the knife and the melanin spot meet
        x == x2 + vs*tskj % Knife position after cutting
        xf == xf0 + vf*(t+tskj); % Position of the melanin spot after it has been cut
        ];
[xs,x1s,x2s,xfs,v1s,ts,t1s,t2s] = vpasolve(Eqns, [x,x1,x2,xf,v1,t,t1,t2]);
end

%% Function: deleting impossible solution and updating variables
% Every variable has two solutions from the set of equation. Only one is
% possible, as time cannot be negative.
% This function deletes the impossible solution.
function [xs, x1s, x2s, xfs, v1s, ts, t1s, t2s, T] = solution_picker(xs, x1s, x2s, xfs, v1s, ts, t1s, t2s, aks)
    if ((ts(1) < 0) && (ts(2) < 0)) || ((t1s(1) < 0) && (t1s(2) < 0)) || ((t2s(1) < 0) && (t2s(2) < 0))
        fprintf('Impossible situation with an acceleration of: %i m/s^2\n', aks);
        T = false;
        xs = 0;
        x1s = 0;
        x2s = 0;
        xfs = 0;
        v1s = 0;
        ts = 0;
        t1s = 0;
        t2s = 0;
    elseif (ts(1) < 0) || (t1s(1) < 0) || (t2s(1) < 0) 
        T = true;
        % Deleting impossible solution if applicable, and updating variables:
        xs = xs(2);
        x1s = x1s(2);
        x2s = x2s(2);
        xfs = xfs(2);
        v1s = v1s(2);
        ts = ts(2);
        t1s = t1s(2);
        t2s = t2s(2);
    elseif (ts(2) < 0) || (t1s(2) < 0) || (t2s(2) < 0)
        T = true;
        % Deleting impossible solution if applicable, and updating variables:
        xs = xs(1);
        x1s = x1s(1);
        x2s = x2s(1);
        xfs = xfs(1);
        v1s = v1s(1);
        ts = ts(1);
        t1s = t1s(1);
        t2s = t2s(1);
    end
end

%% Function: check if legal position
% Checking if the position is within the limits of the workspace
% Checking if the current speed allows for stopping within the limits of
% the workspace with maximum deceleration.
function [legal_pos, stopping_position] = legal_position(aks, xs, x_lim, vs, x_turning)
    % Calculating the position where the knife stops with max acceleration
    if (vs > 0)
        stopping_position = (0 - vs^2)/(2*(-aks)) + xs; 
    elseif (vs < 0)
        stopping_position = (0 - vs^2)/(2*(aks)) + xs; 
    elseif (vs == 0)
        stopping_position = xs;
    end
    
    % Checking if the stopping position is within workspace limits
    if (stopping_position > x_lim(1)) && (x_turning > x_lim(1)) && (stopping_position < x_lim(2)) && (x_turning < x_lim(2))
        legal_pos = true;
    else
        legal_pos = false;
    end        
end